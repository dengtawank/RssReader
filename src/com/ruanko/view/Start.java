package com.ruanko.view;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.ruanko.service.UpdateThread;

public class Start {

	public static void main(String[] args)
	{
		// 开启选择样式窗口
		new StyleFrame().setVisible(true);
		// 开始后台更新线程
		UpdateThread updateThread = new UpdateThread();
		updateThread.setDaemon(true);
		updateThread.start();

	}
}

@SuppressWarnings("serial")
class StyleFrame extends JFrame
{
	@SuppressWarnings("rawtypes")
	JComboBox styleList = null;
	JButton setStyle = null;
	private final int WIDTH = 400;
	private final int HEIGHT = 100;
	
	public StyleFrame()
	{
		// 设置参数
		setTitle("选择样式");
		setSize(WIDTH, HEIGHT);
		setLayout(new BorderLayout());
		// 设置中心显示
		Toolkit kit = Toolkit.getDefaultToolkit();
		Dimension screenSize = kit.getScreenSize();
		this.setLocation((screenSize.width - WIDTH)/2, (screenSize.height - HEIGHT)/2);
		// 添加子组件
		add(getMain(), BorderLayout.CENTER);
		// 设置界面关闭事件
		this.addWindowListener(new WindowAdapter()
				{
					@Override
					public void windowClosing(WindowEvent e)
					{
						// 关闭窗口
						setVisible(false);
						// 释放资源
						dispose();
						// 退出程序
						System.exit(0);
					}
				});
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JPanel getMain()
	{
		// 设置父组件
		JPanel main = new JPanel(new GridLayout(2, 1, 10, 10));
		// 设置子组件
		styleList = new JComboBox();
		for (UIManager.LookAndFeelInfo look : UIManager.getInstalledLookAndFeels())
		{
			String fullName = look.getClassName();
			styleList.addItem((fullName.substring(fullName.lastIndexOf(".")+1, fullName.indexOf("L"))));
		}
		styleList.setSelectedIndex(1);
		styleList.setEditable(false);
		

		setStyle = new JButton("确定");
		// 设置事件处理
		// 设置确定按钮事件
		setStyle.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				// 设置皮肤
				try{
					// 寻找对应类名
					for (UIManager.LookAndFeelInfo look : UIManager.getInstalledLookAndFeels())
					{
						if (look.getClassName().contains((CharSequence) styleList.getSelectedItem()))
							UIManager.setLookAndFeel(look.getClassName());
					}
				}catch(Exception e1)
				{
					e1.printStackTrace();
				}
				// 关闭窗口
				setVisible(false);
				// 释放资源
				dispose();

				// 显示主界面
				new JMainFrame().setVisible(true);
				System.out.println("欢迎进入RSS阅读器");
			}
		});
		// 添加子组件
		main.add(styleList);
		main.add(setStyle);
		// 返回父组件
		return main;
	}
}