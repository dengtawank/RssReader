package com.ruanko.model;

public class News {
	private String title;		// 新闻标题
	private String link;		// 新闻链接
	private String author;		// 新闻作者
	private String guid;		// 新闻网址
	private String category;	// 新闻版块
	private String pubDate;		// 新闻发布日期
	private String comments;	// 新闻要文
	private String description;	// 新闻描述

	/**
	 * @param title
	 * @param link
	 * @param author
	 * @param guid
	 * @param category
	 * @param pubDate
	 * @param comments
	 * @param description
	 */
	public News(String title, String link, String author, String guid,
			String category, String pubDate, String comments, String description) {
		this.title = title;
		this.link = link;
		this.author = author;
		this.guid = guid;
		this.category = category;
		this.pubDate = pubDate;
		this.comments = comments;
		this.description = description;
	}
	
	public News()
	{
		this(null,null,null,null,null,null,null,null);
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid) {
		this.guid = guid;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the pubDate
	 */
	public String getPubDate() {
		return pubDate;
	}

	/**
	 * @param pubDate the pubDate to set
	 */
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
