/**
 * 
 */
package com.ruanko.dao.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import com.ruanko.model.Channel;

/**
 * @author newcih
 *
 */
public class ChannelDao {

	// 第一次启动时从文件中读取
	private static ArrayList<Channel> channelList = getChannelFromFile();

	// 添加频道
	public static void addChannel(Channel channel)
	{
		channelList.add(channel);
	}

	// 将频道数据存储到文件，退出程序时调用
	public boolean saveChannelToFile()
	{
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		// 创建文件
		File file = new File("NewsFiles/ChannelDao");

		try {
			// 重置文件内容
			fos = new FileOutputStream(file, false);
			oos = new ObjectOutputStream(fos);

			// 将一个List对象写入
			oos.writeObject(channelList);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 写入失败
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			// 写入失败
			return false;
		}finally{
			try{
				if (fos != null)
					fos.close();
				if (oos != null)
					oos.close();
			}catch(Exception e)
			{}
		}

		// 写入成功
		return true;
	}

	// 第一次启动时调用
	@SuppressWarnings("unchecked")
	public static ArrayList<Channel> getChannelFromFile()
	{
		FileInputStream fis = null; 
		ObjectInputStream ois = null;
		// 临时变量，用以保存列表
		ArrayList<Channel> channelList = null;

		// 创建存放频道列表的文件
		File file = new File("NewsFiles/ChannelDao");
		//
		// 获取数据
		try {
			
			// 创建存放频道列表的文件
			file = new File("NewsFiles/ChannelDao");
			// 第一次启动时不存在该文件
			if (!file.exists())
				try {
					file.createNewFile();
					
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			fis = new FileInputStream(file);
			ois = new ObjectInputStream(fis);
			
			// 读取文件中的频道
			try {
				channelList = (ArrayList<Channel>)ois.readObject();

			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				// 如果读取出错则初始化channelList
				channelList = new ArrayList<Channel>();
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			// 如果读取出错则初始化channelList
			channelList = new ArrayList<Channel>();
			e.printStackTrace();
			
		}finally{
			try{
				if (fis != null)
					fis.close();
				if (ois != null)
					ois.close();
			}catch(Exception e)
			{}
		}

		return channelList;
	}
	// 程序运行时调用
	public ArrayList<Channel> getChannel()
	{
		return channelList;
	}
}
