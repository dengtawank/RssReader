/**
 * 
 */
package com.ruanko.dao.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.ruanko.dao.NewsDao;
import com.ruanko.model.News;
import com.ruanko.service.RSSService;

/**
 * @author new cih
 *
 */
public class FileDaoImpl implements NewsDao {
	//	private static final String filePath = "NewsFiles/rss.txt";

	public boolean save(News news, File  file)
	{
		boolean flag = false;
		FileWriter fw = null;
		BufferedWriter bw = null;

		try {
			fw = new FileWriter(file, false);
			bw = new BufferedWriter(fw);

			// 将news文件写入文件中
			bw.write(RSSService.newsToString(news));
			bw.newLine();
			// 设置标识位
			flag = true;
			
			bw.flush();

			return flag;
		}catch(IOException e)
		{
			e.printStackTrace();
		}finally
		{ // 关闭顺序与打开顺序相反
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return flag;
	}
}
