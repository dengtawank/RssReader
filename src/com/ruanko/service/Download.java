package com.ruanko.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JOptionPane;

import com.ruanko.model.MyToolkits;

public class Download
{
	// value值，用以反馈给进度条
	// 初始化为1,当为0时提示下载错误
	private static int value = 1;

	// 获取value值
	public static int getValue()
	{
		return value;
	}
	// 重置value
	public static void resetValue()
	{
		value = 0;
	}

	// https
	public static File downloadHttps(String strUrl)
	{
		JOptionPane.showMessageDialog(null, "还无法访问https资源");
		return null;
	}

	// 访问http
	public static File downloadHttp(String strUrl)
	{
		value += 10;

		InputStream inputStream = null;
		HttpURLConnection httpURLConnection = null;

		value += 10;
		// 获取InputStream
		try{
			// 如果url无效
			if (!MyToolkits.checkURL(strUrl))
				return null;
			URL url = new URL(strUrl);	

			if (url != null)
			{
				httpURLConnection = (HttpURLConnection)url.openConnection();	
				// 设置连接网络的超时时间
				httpURLConnection.setConnectTimeout(3000);

				value += 10;

				httpURLConnection.setDoInput(true);
				// 表示设置本次http请求使用GET方式请求
				httpURLConnection.setRequestMethod("GET");
				// 200 表示 HTTP_OK
				int responseCode = httpURLConnection.getResponseCode();

				value += 10;

				if (responseCode == 200)
				{
					// 从服务器获得一个输入流
					inputStream = httpURLConnection.getInputStream();
					value += 10;
				}
			}
		}catch(java.net.SocketTimeoutException e0)
		{
			e0.printStackTrace();
			// 捕捉到错误即可视为下载错误
			value = 0;
			return null;
		}catch(MalformedURLException e)
		{
			e.printStackTrace();	
			// 捕捉到错误即可视为下载错误
			value = 0;
			return null;
		}catch(Exception e)
		{
			e.printStackTrace();	
			// 捕捉到错误即可视为下载错误
			return null;
		}


		value += 10;

		// 将InputStream的内容写入文件
		byte[] data = new byte[1024];
		int len = 0;
		File file = null;
		FileOutputStream fileOutputStream = null;

		value += 10;

		try{
			file = new File("NewsFiles"+strUrl.substring(strUrl.lastIndexOf("/"), strUrl.length()));
			// 针对有些URL是文件夹结尾
			if (file.isDirectory())
			{
				JOptionPane.showMessageDialog(null, "暂不支持该类型url");
				return null;
			}
			// 创建文件
			if (!file.exists())
				file.createNewFile();

			fileOutputStream = new FileOutputStream(file);	

			value += 10;

			// 判断inputstream是否为空
			if (inputStream != null)
			{
				while((len = inputStream.read(data)) != -1)
					fileOutputStream.write(data, 0, len);
			}
			value += 10;

		}catch(IOException e)
		{
			e.printStackTrace();	
			// 捕捉到错误即可视为下载错误
			value = 0;
			return null;
		}finally{
			if (inputStream != null)	
			{
				try{
					inputStream.close();			
				}catch(IOException e)	
				{
					e.printStackTrace();	
				}
			}
		}
		// 返回文件对象		
		value += 10;

		return file;
	}
	// 执行下载操作并返回文件对象
	public static File download(String strUrl)
	{
		if (strUrl.startsWith("https"))
			return downloadHttps(strUrl);
		else 
			return downloadHttp(strUrl);
	}
}
