/**
 * 
 */
package com.ruanko.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.ruanko.dao.NewsDao;
import com.ruanko.dao.impl.ChannelDao;
import com.ruanko.dao.impl.FileDaoImpl;
import com.ruanko.model.Channel;
import com.ruanko.model.MyToolkits;
import com.ruanko.model.News;

/**
 * @author new
 */

public class RSSService {

	// 用于保存RSS源文件
	private static ArrayList<Channel> channelList;
	// 表示新闻信息列表
	private List<News> newsList;
	// 创建数据访问层对象
	private static NewsDao rssDao;
	// 创建数据访问层对象
	private static ChannelDao channelDao;

	static{
		rssDao = new FileDaoImpl();
		channelDao = new ChannelDao();
		// 获取初始化列表
		channelList = (ArrayList<Channel>) channelDao.getChannel();
	}

	// 无参构造方法
	public RSSService()
	{

	}

	// 调用数据访问层的方法，保存新闻内容
	public boolean saveNews(News news, File file)
	{
		boolean flag = false;
		// 检测输入参数是否有错
		if (file == null || !file.exists() || file.isDirectory())
			return false;
		if (news == null)
			return false;
		// 确认无误则进行写入导出
		if (rssDao.save(news, file))
			flag = true;

		return flag;
	}
	// 调用数据访问层方法，保存频道内容
	public boolean saveChannel()
	{
		boolean flag = false;

		if (channelDao.saveChannelToFile())
			flag = true;
		return flag;
	}
	// 通过URL路径下载XML文件
	public File downloadXML(String strUrl)
	{
		return Download.download(strUrl);
	}
	// 添加RSS频道
	@SuppressWarnings("static-access")
	public void addChannel(Channel channel)
	{
		if (channel != null)
			channelDao.addChannel(channel);
	}
	// 删除RSS频道
	public boolean deleteChannel(String channelName)
	{
		// 未传入channelName
		if (channelName == null)
			return false;
		
		for (int i = 0; i < channelList.size(); i++)
		{
			// 名字匹配成功
			if (channelList.get(i).getName().equals(channelName))
			{
				// 执行删除
				channelList.remove(i);				
				return true;
			}
		}
		// 否则返回失败
		return false;
	}
	// 编辑RSS频道
	public boolean setChannel(String oldChannelName, String newChannelName, String channelURL)
	{
		if (oldChannelName == null || newChannelName == null || channelURL == null)
			return false;
		
		for (int i = 0; i < channelList.size(); i++)
		{
			// 名字匹配成功
			if (channelList.get(i).getName().equals(oldChannelName))
			{
				channelList.set(i, new Channel(newChannelName, channelURL));				
				return true;
			}
		}
		// 否则返回失败
		return false;
	}
	// 获取RSS频道
	public ArrayList<Channel> getChannelList()
	{
		return channelList;
	}

	// 将XML文件解析为Document对象
	public Document load(File fXml) throws JDOMException, IOException
	{
		if (fXml == null || fXml.isDirectory() || !fXml.exists())
			return null;
		
		SAXBuilder sb = null;
		Document doc = null;
		
		try{
			sb = new SAXBuilder(false);
			doc = sb.build(fXml);
		} catch(JDOMException e)
		{
			e.printStackTrace();
			// 捕捉到错误就返回null
			// return null;
		} catch(IOException e1)
		{
			e1.printStackTrace();
			// 捕捉到错误就返回null
			// return null;
		}
		return doc;
	}

	// 代码作者 ： 软件2班 刘广升

	// 将RSS文件中的item元素转化为News对象
	private News itemToNews(Element item)
	{
		News news = new News();
		// 获得节点内容
		String title = item.getChildText("title").trim();
		String link = item.getChildText("link");
		String author = item.getChildText("author");
		String guid = item.getChildText("guid");
		String category = item.getChildText("category");
		String pubDate = item.getChildText("pubDate");
		String comments = item.getChildText("comments");
		String description = item.getChildText("description");
		// 设置节点内容
		news.setTitle(title);
		news.setLink(link);
		news.setAuthor(author);
		news.setGuid(guid);
		news.setCategory(category);
		news.setPubDate(pubDate);
		news.setComments(comments);
		news.setDescription(description);

		return news;
	}

	// 解析内存中的RSS文件，将生成新闻对象News列表
	@SuppressWarnings("unchecked")
	private List<News> parse(Document doc)
	{
		newsList = new ArrayList<News>();
		News news = null;

		Element root = doc.getRootElement();
		Element eChannel = root.getChild("channel");
		List<Element> itemList = eChannel.getChildren("item");

		for (int i = 0; i < itemList.size(); i++)
		{
			Element item = itemList.get(i);
			news = itemToNews(item);
			newsList.add(news);
		}

		return newsList;
	}

	// 代码作者 ： 软件2班 刘广升
	// 获得新闻内容信息列表
	public List<News> getNewsList(File file) throws JDOMException, IOException
	{
		Document doc = load(file);
		// 解析xml文件成功
		if (doc != null)
		{
			newsList = parse(doc);
			return newsList;
		}
		return null;
	}


	// 实现将新闻实体类转换为字符串
	public static String newsToString(News news)
	{
		String content = null;

		content = "标题: "
				+ news.getTitle()
				+ "\r\n"
				+ "链接: "
				+ news.getLink()
				+ "\r\n"
				+ "作者: "
				+ news.getAuthor()
				+ "\r\n"
				+ "发布时间: "
				+ news.getPubDate()
				+ "\r\n"
				+ "=========================================================================\r\n"
				+ news.getDescription()
				+ "\r\n"
				+ "\r\n"
				+ "\r\n";

		return content;
	}
}
