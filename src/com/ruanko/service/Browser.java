/**
 * 
 */
package com.ruanko.service;

/**
 * @author new cih
 *
 */
import java.util.Arrays;    
import javax.swing.JOptionPane;    

// 通过调用计算机本地浏览器实现浏览网页功能
public class Browser{    
	static final String[] browsers = { "google-chrome", "firefox", "opera",    
		"epiphany", "konqueror", "conkeror", "midori", "kazehakase",    
		"mozilla" };    
	static final String errMsg = "Error attempting to launch web browser";    

	public static void openURL(String url) {    
		try { 
			// 加载本地浏览器
			Class<?> d = Class.forName("java.awt.Desktop");    
			d.getDeclaredMethod("browse", new Class[] { java.net.URI.class })    
				.invoke(d.getDeclaredMethod("getDesktop").invoke(null),    
						new Object[] { java.net.URI.create(url) });    
		} catch (Exception ignore) { 
			// 如果加载不到浏览器
			String osName = System.getProperty("os.name");    
			try {    
				if (osName.startsWith("Mac OS")) {    
					Class.forName("com.apple.eio.FileManager")    
						.getDeclaredMethod("openURL",    
								new Class[] { String.class })    
						.invoke(null, new Object[] { url });    
				} else if (osName.startsWith("Windows"))    
					Runtime.getRuntime().exec(    
							"rundll32 url.dll,FileProtocolHandler " + url);    
				else { 
					String browser = null;    
					for (String b : browsers)    
						if (browser == null    
								&& Runtime.getRuntime()    
								.exec(new String[] { "which", b })    
								.getInputStream().read() != -1)    
							Runtime.getRuntime().exec(    
									new String[] { browser = b, url });    
					if (browser == null)    
						throw new Exception(Arrays.toString(browsers));    
				}    
			} catch (Exception e) {    
				JOptionPane.showMessageDialog(null,    
						errMsg + "\n" + e.toString());    
			}    
		}    
	}    
}
